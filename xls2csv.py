# Модифицированный пакет xlrd для чтения поврежденных файлов xls

import pathlib
import csv
import tempfile
from optparse import OptionParser
import xlrd
import codecs
from tablepyxl import tablepyxl


def trim(item):
    for index, value in enumerate(item):
        if (type(value) == str):
            for substr in ["\n", "\t", "\r"]:
                item[index] = item[index].replace(substr, "")
    return item


# Вспомогательные функции
textchars = bytearray({7, 8, 9, 10, 12, 13, 27} | set(range(0x20, 0x100)) - {0x7f})
is_binary_string = lambda bytes: bool(bytes.translate(None, textchars))


# Очень тупой метод для определения кодировки файла
# Читаем одну линию и если получаем исключение UnicodeDecodeError, то это windows-1251; иначе = utf-8
def detect_encoding(file):
    f = open(file, "r")

    try:
        f.readline()
    except UnicodeDecodeError:
        return "windows-1251"

    return "utf-8"


# Текстовый или двоичный?
def is_binary(file):
    return is_binary_string(open(file, 'rb').read(1024))


def html2xls(file):
    f = open(file, "r", encoding=detect_encoding(file))
    xls = tempfile._get_default_tempdir() + "/" + next(tempfile._get_candidate_names())
    tablepyxl.document_to_xl(f.read(), xls)
    return xls


parser = OptionParser(usage="Usage: %prog SOURCE DESTINATION [options]")
parser.add_option("-d", "--delimiter", dest="csv_delimiter", default="\t",
                  help="Delimiter for CSV (Default: Tab)")
parser.add_option("-s", "--sheet", dest="sheet_name", default="",
                  help="Sheet name (Default first sheet)")
parser.add_option("-a", dest="skip_assert", action="store_true", default=False,
                  help="Alternative read mode (if normal mode not working)")
parser.add_option("-e", "--encoding", dest="encoding", default=None,
                  help="XLS encoding")
# parser.add_option("-q", "--quotechar", dest="csv_quotechar", default='"',
#                   help="Quote char for CSV (Default: \")")

(options, args) = parser.parse_args()

try:
    source = args[0]
except IndexError:
    print('No input file')
    exit(1)

try:
    destination = args[1]
except IndexError:
    destination = pathlib.Path(source).stem + '.csv'

# Если файл не двоичный, то думаем, что это HTML и пытаемся преобразовать его в нормальный XLS
if (not is_binary(source)):
    source = html2xls(source)

wb = xlrd.open_workbook(source,
                        encoding_override=options.encoding,
                        formatting_info=False,
                        skip_assert=options.skip_assert)
# exit(0)
sheet_count = len(wb.sheet_names())

# Используем параметр sheet_name только если кол-во листов больше 1
if (options.sheet_name != '' and sheet_count > 1):
    sh = wb.sheet_by_name(options.sheet_name)
else:
    sh = wb.sheet_by_index(0)

fh = codecs.open(destination, 'wb', 'utf-8-sig')
csv_out = csv.writer(fh, delimiter=options.csv_delimiter)

# Кривые файлы могут отдавать nrows = 0, пытаемся перебирать пока не будет ошибки
rows = sh.nrows if sh.nrows > 0 else 1000000

for row_number in range(rows):
    # Пытаемся получить строку. Если ошибка - прерываем цикл
    try:
        row_values = sh.row_values(row_number)
    except Exception:
        row_values = None

    if (row_values == None):
        break

    row_types = sh.row_types(row_number)
    for (i, v) in enumerate(row_values):
        # Костыль для ячеек типа Number и текстовых значений. Например, производитель 555 читается как 555.0
        try:
            if (row_types[i] == 2):
                if round(row_values[i]) == row_values[i]:
                    row_values[i] = str(round(row_values[i]))
            if (row_types[i] == 5):
                row_values[i] = ''
        except Exception:
            pass

    csv_out.writerow(trim(row_values))

fh.close()

exit(0)
