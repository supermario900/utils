import csv
from optparse import OptionParser
from xlsxwriter.workbook import Workbook

parser = OptionParser(usage="Usage: %prog SOURCE DESTINATION [options]")
parser.add_option("-c", "--charset", dest="csv_charset", default='windows-1251',
                  help="Charset for CSV (Default: windows-1251)")
parser.add_option("-d", "--delimiter", dest="csv_delimiter", default=';',
                  help="Delimiter for CSV (Default: ;)")
parser.add_option("-q", "--quotechar", dest="csv_quotechar", default='"',
                  help="Quote char for CSV (Default: \")")

(options, args) = parser.parse_args()

try:
    source = args[0]
except IndexError:
    print('No input file')
    exit()

try:
    destination = args[1]
except IndexError:
    destination = source[:-4] + '.xlsx'

workbook = Workbook(destination)
worksheet = workbook.add_worksheet()
with open(source, 'rt', encoding=options.csv_charset) as f:
    reader = csv.reader(f, delimiter=options.csv_delimiter, quotechar=options.csv_quotechar)
    for r, row in enumerate(reader):
        for c, col in enumerate(row):
            worksheet.write(r, c, col)
workbook.close()