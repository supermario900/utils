## Необходимые пакеты
pip3 install xlsxwriter

pip3 install xlrd

pip3 install pathlib

## csv2xlsx
Использование: 
`csv2xlsx SOURCE DESTINATION [options]`

`SOURCE` Имя CSV файла

`DESTINATION` Имя XLSX файла (необязательно)

`-c CSV_CHARSET, --charset=CSV_CHARSET` Charset for CSV (Default: windows-1251)
                        
`-d CSV_DELIMITER, --delimiter=CSV_DELIMITER`
Delimiter for CSV (Default: ;)
                        
`-q CSV_QUOTECHAR, --quotechar=CSV_QUOTECHAR`
Quote char for CSV (Default: ")
                        

## xls2csv
Использование: 
`xls2csv SOURCE DESTINATION [options]`

`SOURCE` Имя XLS файла

`DESTINATION` Имя CSV файла (необязательно)
                        
`-d CSV_DELIMITER, --delimiter=CSV_DELIMITER`
Delimiter for CSV (Default: Tab)